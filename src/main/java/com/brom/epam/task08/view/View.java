package com.brom.epam.task08.view;

import com.brom.epam.task08.controller.Controller;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private Controller controller;

  public View() {
    initMenu();
    initMenuCommands();
    controller = new Controller();
  }

  private void initMenu() {
    this.menu = new HashMap<>();
    this.menu.put(MenuConstants.PING_PONG_KEY, MenuConstants.PING_PONG_TEXT);
    this.menu.put(MenuConstants.FIBONACCI_KEY, MenuConstants.FIBONACCI_TEXT);
    this.menu.put(MenuConstants.FIBONACCI_WITH_EXECUTORS_KEY,
        MenuConstants.FIBONACCI_WITH_EXECUTORS_TEXT);
    this.menu.put(MenuConstants.FIBONACCI_CALLABLE_KEY,
        MenuConstants.FIBONACCI_CALLABLE_TEXT);
    this.menu.put(MenuConstants.RANDOM_SLEEP_KEY,
        MenuConstants.RANDOM_SLEEP_TEXT);
    this.menu.put(MenuConstants.MONITOR_KEY,
        MenuConstants.MONITOR_TEXT);
    this.menu.put(MenuConstants.PIPE_KEY,
        MenuConstants.PIPE_TEXT);
    this.menu.put(MenuConstants.QUIT_KEY,
        MenuConstants.QUIT_TEXT);
  }

  private void initMenuCommands() {
    this.menuCommands = new HashMap<>();
    this.menuCommands.put(MenuConstants.PING_PONG_KEY, this::pingPongCommand);
    this.menuCommands.put(MenuConstants.FIBONACCI_KEY, this::fibonacciCommand);
    this.menuCommands.put(MenuConstants.FIBONACCI_WITH_EXECUTORS_KEY, this::fibonacciWithExecutorsCommand);
    this.menuCommands.put(MenuConstants.FIBONACCI_CALLABLE_KEY, this::fibonacciNumbersSum);
    this.menuCommands.put(MenuConstants.MONITOR_KEY, this::showMonitorExample);
    this.menuCommands.put(MenuConstants.PIPE_KEY, this::showPipedThreadsExample);
    this.menuCommands.put(MenuConstants.QUIT_KEY, this::quit);
  }

  private void pingPongCommand() {
    controller.executePingPongTask();
  }

  private void fibonacciWithExecutorsCommand() {
    logger.info("Enter please fibonacci number index");
    int index = input.nextInt();
    input.nextLine();
    this.controller.findFibonacciNumberWithExecutors(index);
  }

  private void quit() {
    System.exit(0);
  }

  private void showPipedThreadsExample() {
    controller.showPipedThread();
  }

  private void showMonitorExample() {
    controller.executeCriticalSectionExample();
  }

  private void fibonacciNumbersSum() {
    logger.info("Enter index of fibonacci number");
    int startIndex = input.nextInt();
    input.nextLine();
    logger.info("Enter amount of fibonacci numbers for summing");
    int amountOfNumbers = input.nextInt();
    input.nextLine();
    int sum = this.controller.getSumOfFibonacciNumbers(startIndex, amountOfNumbers);
    logger.info(sum);
  }

  private void randomSleepThreads() {
    logger.info("Enter number of threads");
    int numberOfThreads = input.nextInt();
    input.nextLine();
    controller.executeRandomSleepThreads(numberOfThreads);
  }

  private void fibonacciCommand() {
    logger.info("Enter please fibonacci number index");
    int index = input.nextInt();
    input.nextLine();
    this.controller.findFibonacciNumber(index);
  }

  public void show() {
    while (true) {
      menu.values().forEach(v -> logger.info(v));
      String command = input.nextLine();
      this.menuCommands.get(command).execute();
    }
  }
}
