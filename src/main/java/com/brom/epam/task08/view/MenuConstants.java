package com.brom.epam.task08.view;

public class MenuConstants {
  public static final String PING_PONG_KEY = "1";
  public static final String FIBONACCI_KEY = "2";
  public static final String FIBONACCI_WITH_EXECUTORS_KEY = "3";
  public static final String FIBONACCI_CALLABLE_KEY = "4";
  public static final String RANDOM_SLEEP_KEY = "5";
  public static final String MONITOR_KEY = "6";
  public static final String PIPE_KEY = "7";
  public static final String QUIT_KEY = "8";
  public static final String PING_PONG_TEXT = "1. Start ping pong task";
  public static final String FIBONACCI_TEXT = "2. Calculate fibonacci number";
  public static final String FIBONACCI_WITH_EXECUTORS_TEXT = "3. Calculate fibonacci number using executors";
  public static final String FIBONACCI_CALLABLE_TEXT = "4. Calculate sum of fibonacci numbers with callable";
  public static final String RANDOM_SLEEP_TEXT = "5. Random sleep thread";
  public static final String MONITOR_TEXT = "6. Monitor threads";
  public static final String PIPE_TEXT = "7. Piped threads";
  public static final String QUIT_TEXT = "8. Quit";

}
