package com.brom.epam.task08.view;

public interface Command {
  void execute();
}
