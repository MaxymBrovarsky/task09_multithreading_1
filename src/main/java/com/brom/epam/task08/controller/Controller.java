package com.brom.epam.task08.controller;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
  private Object sync = new Object();
  private Logger logger = LogManager.getLogger(Controller.class.getName());
  private Random random = new Random();
  public void executePingPongTask() {
    Thread pingThread = getPingPongThread("Ping");
    Thread pongThread = getPingPongThread("Pong");
    pingThread.start();
    pongThread.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      logger.error(e);
    }
    pingThread.interrupt();
    pongThread.interrupt();
  }

  private Thread getPingPongThread(String text) {
    return new Thread(() -> {
      synchronized (sync) {
        while (!Thread.interrupted()) {
          try {
            sync.notify();
            logger.info(text);
            sync.wait();
          } catch (InterruptedException e) {
          }
        }
      }
    });
  }

  public void findFibonacciNumber(int index) {
    for (int i = 0; i < 5; i++) {
      new Thread(() -> logger.info(calculateFibonacciNumber(index + random.nextInt(10)))).start();
    }
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {

    }
  }

  public void findFibonacciNumberWithExecutors(int index) {
    final int threadsNumber = 8;
    logger.info("----------fixedThreadPoolExecutor-----------");
    ExecutorService fixedExecutorService = Executors.newFixedThreadPool(threadsNumber);
    for (int i = 0; i < threadsNumber; i++) {
      fixedExecutorService.submit(getFibonacciRunnable(index + i));
    }
    logger.info("----------singleThreadPoolExecutor-----------");
    ExecutorService singleThreadExecutorService = Executors.newSingleThreadExecutor();
    for (int i = 0; i < threadsNumber; i++) {
      singleThreadExecutorService.submit(getFibonacciRunnable(index + i));
    }
    logger.info("----------singleThreadPoolScheduledExecutor-----------");
    ScheduledExecutorService singleThreadScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    for (int i = 0; i < threadsNumber; i++) {
      singleThreadScheduledExecutorService.schedule(
          getFibonacciRunnable(index + i),1, TimeUnit.SECONDS);
    }
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {

    }
    fixedExecutorService.shutdown();
    singleThreadExecutorService.shutdown();
    singleThreadScheduledExecutorService.shutdown();
  }

  private Runnable getFibonacciRunnable(int index) {
    return () -> {
      logger.info("Thread " + Thread.currentThread().getId() + " System start nanotime " + System.nanoTime());
      logger.info(calculateFibonacciNumber(index));
      logger.info("Thread " + Thread.currentThread().getId() + " System end nanotime " + System.nanoTime());
    };
  }

  public int getSumOfFibonacciNumbers(int startIndex, int amountOfNumbers) {
    Callable<Integer> sumOfFibonacciNumbers = () -> {
      int sum = 0;
      for (int i = 0; i < amountOfNumbers; i++) {
        sum += calculateFibonacciNumber(startIndex + i);
      }
      return sum;
    };
    try {
      return sumOfFibonacciNumbers.call();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;
  }

  public void executeRandomSleepThreads(int numberOfThreads) {
    for (int i = 0; i < numberOfThreads; i++) {
      new Thread(() -> {
        int sleepTime = random.nextInt(10) + 1;
        logger.info("Start thread with " + sleepTime + " seconds sleep");
        try {
          Thread.sleep(sleepTime * 1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        logger.info("Exit thread with " + sleepTime + " seconds sleep");
      }).start();
    }
  }

  public void executeCriticalSectionExample() {
    logger.info("-----------one monitor----------------");
    criticalSectionWithOneMonitor();
    logger.info("-----------few monitor----------------");
    criticalSectionWithFewMonitor();
  }

  private void criticalSectionWithOneMonitor() {
    Object monitor = new Object();
    AtomicInteger a = new AtomicInteger();
    Thread thread1 = new Thread(() -> {
      synchronized(monitor) {
        a.addAndGet(10);
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
    Thread thread2 = new Thread(() -> {
      synchronized(monitor) {
        a.addAndGet(10);
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
    Thread thread3 = new Thread(() -> {
      synchronized(monitor) {
        a.addAndGet(10);
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
    thread1.start();
    thread2.start();
    thread3.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info(a);
  }

  private void criticalSectionWithFewMonitor() {
    Object monitor1 = new Object();
    Object monitor2 = new Object();
    Object monitor3 = new Object();
    AtomicInteger a = new AtomicInteger();
    Thread thread1 = new Thread(() -> {
      synchronized(monitor1) {
        a.addAndGet(10);
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
    Thread thread2 = new Thread(() -> {
      synchronized(monitor2) {
        a.addAndGet(10);
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
    Thread thread3 = new Thread(() -> {
      synchronized(monitor3) {
        a.addAndGet(10);
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    });
    thread1.start();
    thread2.start();
    thread3.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info(a);
  }

  private int calculateFibonacciNumber(int index) {
    if (index <= 1) {
      return index;
    }
    return calculateFibonacciNumber(index-1) + calculateFibonacciNumber(index - 2);
  }

  public void showPipedThread() {
    String text = "Some text to transfer between threads\n  ";
    PipedInputStream pipedInputStream = new PipedInputStream();
    PipedOutputStream pipedOutputStream = new PipedOutputStream();
    try {
      pipedInputStream.connect(pipedOutputStream);
    } catch (IOException e) {
      logger.error(e);
    }
    Thread writerThread = getOutputPipeThread(pipedOutputStream, text);
    Thread readerThread = getInputPipeThread(pipedInputStream);
    writerThread.start();
    readerThread.start();
    try {
      writerThread.join();
      readerThread.join();
    } catch (InterruptedException e) {
      logger.error(e);
    }
  }

  private Thread getInputPipeThread(PipedInputStream pipedInputStream) {
    return new Thread(() -> {
      int c;
      try {
        while ((c = pipedInputStream.read()) != -1) {
          System.out.print((char)c);
          Thread.sleep(100);
        }
        pipedInputStream.close();
      } catch (IOException | InterruptedException e) {
        logger.error(e);
      }
    });
  }

  private Thread getOutputPipeThread(PipedOutputStream pipedOutputStream, String text) {
    return new Thread(() -> {
      try {
        for (int c: text.toCharArray()) {
          pipedOutputStream.write(c);
        }
        pipedOutputStream.close();
      } catch (IOException e) {
        logger.error(e);
      }
    });
  }

}
